import Client from "./Client.js";
import config from "../../config.json";

window.client = new Client({
  endpoint: config.ENDPOINT_OVERRIDE || `${window.location.origin}/api`,
  token: window.localStorage.getItem("token")
});

window.profilePromise = window.client
  .getProfile()
  .then(r => r)
  .catch(err => {
    if (err.errorCode == "BAD_AUTH") {
      console.log("Unsetting token due to BAD_AUTH");
      localStorage.setItem("token", "");
      localStorage.removeItem("token");
      window.client.token = null;
    } else {
      console.log("Something else went wrong in getting our profile!", err);
    }
  });

const loadEvent = new Event("clientLoad");
window.dispatchEvent(loadEvent);
