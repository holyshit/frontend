import "@/styles/token.scss";
import uploaders from "./uploaders.js";
import "highlightjs/styles/solarized-light.css";

const highlightPromise = import("highlightjs/highlight.pack.js");
window.addEventListener("load", async function() {
  function download(link, data, mime, filename) {
    // Everything we need supports Blobs sooo :shrugz:
    const blob = new Blob([data], { type: mime });
    if (navigator.msSaveBlob) {
      link.addEventListener("click", function() {
        navigator.msSaveBlob(blob, filename);
      });
    } else {
      link.href = URL.createObjectURL(blob);
      link.download = filename;
    }
  }

  const token = window.location.hash.substring(1);
  document.getElementById("token").innerText = token;
  const kshareConfig = uploaders.kshareConfig(token);
  const sharexConfig = uploaders.sharexConfig(token);

  document.getElementById("kshare-config").innerText = kshareConfig;
  document.getElementById("sharex-config").innerText = sharexConfig;

  download(
    document.getElementById("kshare-dl"),
    kshareConfig,
    "application/json",
    "elixire.uploader"
  );
  const sharexDl = document.getElementById("sharex-dl");
  download(
    document.getElementById("sharex-dl"),
    sharexConfig,
    "application/json",
    "elixire.sxcu"
  );
  // This way we can still await the highlight deps
  window.profilePromise.then(() => {
    const elixireManager = uploaders.elixireManager(token);
    download(
      document.getElementById("elixire-manager-dl"),
      elixireManager,
      "text/x-shellscript",
      "elixiremanager.sh"
    );
  });

  const highlight = await highlightPromise;

  highlight.highlightBlock(document.getElementById("sharex-config"));
  highlight.highlightBlock(document.getElementById("kshare-config"));
});
